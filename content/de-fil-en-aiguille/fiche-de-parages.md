---
title: "Fiche De Parages"
date: 2019-01-22T13:25:34+01:00
logo: "fa-circle"
couleur: "vert"
image: "05.jpg"
weight: 5
draft: false
---

Le parage correspond à une opération de désépaississement du cuir sur un ou des bords d'une pièce, pour faciliter la couture et permettre un rendu d'article plus fin et soigné. Je réalise une fiche recapitulative où sont annotés tous les parages sur chacune des pièces en cuir de l'article à réaliser. La présence ou l'absence de parage, ainsi que les mesures de parages, dépendent des montages choisis.
