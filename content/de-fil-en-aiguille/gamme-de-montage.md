---
title: "Gamme De Montage"
date: 2019-01-22T13:28:16+01:00
logo: "fa-circle"
couleur: "vert"
image: "01.jpg"
weight: 6
draft: false
---

Je liste dans l'ordre chronologique les différentes étapes de  montage de l'article pour n'en oublier aucune (étapes décrites en suivant). Cela me permet degarder une trace de l'assemblage si je veux ensuite reproduire l'article.

Cette étape est importante pour m'assurer que j'ai bien pensé à tous les éléments constituant l'article et à toutes ses phases d'assemblage et éviter d'avoir à découdre/démonter des pièces par oubli d'intégration d'un élément au préalable.

Cela aide aussi lorsque je veux refaire un article que je n'ai pas réalisé depuis longtemps. Cette étape permet un gain de temps.
