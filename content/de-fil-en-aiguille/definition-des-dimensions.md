---
title: "Definition Des Dimensions"
date: 2019-01-22T13:16:43+01:00
logo: "fa-bug"
couleur: "rouge"
image: "02.jpg"
weight: 2
draft: false
---

Définition des dimensions / mesures / proportions précises de l'article, en accord avec le dessin d'art.
