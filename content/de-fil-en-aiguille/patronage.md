---
title: "Patronage"
date: 2019-01-22T18:09:35+01:00
logo: "fa-circle"
couleur: "vert"
image: "02.jpg"
weight: 8
draft: false
---

Une fois que mon prototype m'a permis de définir les bonnes dimensions, le style précis de l'article et tous ses aspects, je passe au patronage.

Il s'agit de dessiner des  gabarits en cartonnette de chacune des pièces nécessaires à la réalisation de l' article, en prévoyant des marges de couture.

Sur chaque gabarit, je note le nom de l'article dont il fait partie, la zone de l'article à laquelle il correspond (corps, soufflet, fond ...), la matière dans laquelle il doit être coupé (cuir, tissu pour les doublures, synderme pour les renforts), le nombre de pièces nécessaires de même gabarit et les parages nécessaires aux futures coutures. Je rajoute aussi des repères indiquant où seront fixés les accessoires (rivets, pressions, tops magnétiques...).

Les patrons me permettent de "garder en mémoire" les formes et les dimensions de chaque pièce composant les articles et les montages chois, le nombre de patrons nécessaires est très variable.
