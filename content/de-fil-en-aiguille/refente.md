---
title: "Refente"
date: 2019-01-24T04:29:32+01:00
logo: "fa-circle"
couleur: "vert"
image: "05.jpg"
weight: 11
draft: false
---

C'est une étape nécessaire si le cuir utilisé est trop épais. Elles consiste à désépaissir chaque pièce de cuir sur toute sa surface, avec une machine spécifique appelée une refendeuse. Cette machine se règle pour obtenir l'épaisseur voulue.

Cela peut permettre de réaliser un travail plus fin et un article moins lourd: certains cuirs nécessitent d'être appliqués ensemble selon les montages notamment en petite maroquinerie par exemple.

S'ils n'étaient pas refendus, le travail serait plus compliqué, le rendu serait plus grossier et l'article ne serait pas forcément fonctionnel: un cuir trop épais pour un portefeuille par exemple rendrait plus difficile son pliage.

Certaines tanneries offrent la possibilité de refendre le cuir à l'épaisseur souhaitée avant achat. Sinon, il faut que je sous-traite cette opération, car je ne suis pas équipée de refendeuse à l'atelier.
