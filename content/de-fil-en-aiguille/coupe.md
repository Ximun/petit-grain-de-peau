---
title: "Coupe"
date: 2019-01-24T04:26:52+01:00
logo: "fa-circle"
couleur: "vert"
image: "04.jpg"
weight: 10
draft: false
---

Avec mon tranchet, je coupe manuellement les différentes pièces en cuir de l'article. Je m'aide du réglet pour les coupes droites.

Certains maroquiniers ont de grosses presses (mécaniques, hydrauliques ou numériques) pour réaliser la coupe des différentes pièces de cuir avec des emporte-pièces en métal. Il existe aussi un autre procédé de découpe laser.

Ces systèmes de coupe représentent un gain de temps (très) important dans les étapes de production.

A l'atelier, je n'en suis pas équipée et je réalise toute la coupe manuellement.
