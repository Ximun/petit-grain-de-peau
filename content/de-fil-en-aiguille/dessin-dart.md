---
title: "Dessin Dart"
date: 2019-01-22T12:35:50+01:00
logo: "fa-circle"
couleur: "vert"
image: "01.jpg"
weight: 1
draft: false
---

Je dessine l'article que je souhaite réaliser en réfléchissant à:

* Son style
* Sa forme
* Son "tombé" (souple/rigide)
* Ses proportions et dimensions
* Sa/ses couleur(s)
* Le type de cuir selon ce que je souhaite utiliser et qui semble le plus adapté aux effets recherchés (cuir de tannage végétal ou cuir tannage au chrome, cuir pleine fleur aniline, cuir semi-aniline, cuir gras, cuir pigmenté, ...)
* Ses finitions
* Ses accessoires
* Les montages nécessaires pour donner le rendu souhaité
* Éventuellement les accessoires que je pourrais créer pour s'accorder au sac et lui donner un cachet supplémentaire (ceinture, porte-clés...)

Cela me permet de commencer à bien m'imprégner du modèle et de son style pour préparer les étapes de travail suivantes.

Pour cette étape de création, j'aime beaucoup aller dessiner dans les cafés ou dans de jolis coins de nature... Aors que je réalise le reste des étapes dans mon atelier, par rapport aux outils que j'ai besoin d'utiliser.
