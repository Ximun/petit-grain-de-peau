---
title: "Prototypage"
date: 2019-01-22T18:05:26+01:00
logo: "fa-circle"
couleur: "vert"
image: "01.jpg"
weight: 7
draft: false
---

Je réalise un modèle de l'article (une sorte de brouillon) pour en vérifier les proportions, modifier certaines dimensions si besoin, m'assurer qu'il correspond à ce que je veux réaliser.

J'utilise du cuir de récupération pour réaliser les prototypes (vieux pantalons en cuire, anciennes vestes...) pour limiter au maximum l'utilisation des ressources, tout en me permettant d'avoir une idée précise du rendu final de l'article (il me semble pour cela important d'utiliser la même matière (cuir) pour le prototypage et pour le sac final).

En général, je recycle mes prototypes comme trousses/sacs de rangement pour mes accessoires ou j'en donne à des amis artisans pour leurs rangements, afin que ces objets puissent continuer à avoir une utilité.
