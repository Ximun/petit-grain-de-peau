---
title: "Nomenclature"
date: 2019-01-22T13:20:41+01:00
logo: "fa-circle"
couleur: "vert"
image: "03.jpg"
weight: 3
draft: false
---

Je définis la liste des différentes pièces nécessaires à la réalisation de l'article (ex: corps, soufflet, fond, boucle, rivet ...) en indiquant leur nom, leur quantité, leur matière (cuir, tissu pour doublure, métal pour accessoires...).

Je référence chaque pièce par un numéro.
