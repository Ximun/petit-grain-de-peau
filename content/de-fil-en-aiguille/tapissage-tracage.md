---
title: "Tapissage Traçage"
date: 2019-01-24T04:19:34+01:00
logo: "fa-circle"
couleur: "vert"
image: "03.jpg"
weight: 9
draft: false
---

Lorsque mes patrons sont prêts et que j'ai choisi la ou les peaux nécessaires à la confection de l'article, j'étudie les plus belles parties du cuir, ses défauts que je mettrai de côté (trous, coutelures...) pour ne pas fragiliser l'article, les zones particulières que je veux utiliser et mettre en valeur (rides, veines...) ainsi que son prêtant (la capacité de certaines zones de la peau à s'étirer et à rester dans la forme ainsi obtenue) afin d'être attentive à ce que les pièces de l'article ne risquent pas de s'étirer/se déformer à l'usage.

Je peux ainsi avoir une vision globale des parties de la peau que je veux utiliser pour l'article, en faisant en sorte de gaspiller le moins possible de cuir, par respect pour l'animal, mais aussi parce qu'il s'agit d'une matière coûteuse.

Parfois, je trouve que certaines marques telles que des cicatrices (qui pourraient être considérées comme des défauts pour certains) ont un charme incroyable et je prends alors le parti de m'en servir pour les mettre en valeur et donner un style particulier à l'article, voire un vrai caractère.

La cicatrice peut alors devenir l'élément principal d'un sac. Cela peut lui donner par exemple un aspect brut, ou l'aspect d'un objet ancien...

Lorsque j'ai bien observé et étudié la peau dans son ensemble, je dispose dessus les patrons de l'article à réaliser (sur les zones choisies) : c'est le tapissage.

Ensuite, je trace les contours des patrons sur le cuir avec un crayon d'argent (sa particularité est de pouvooir se gommer sur la plupart des cuirs). C'est l'étape du traçage.
