---
title: "Dessin Technique"
date: 2019-01-22T13:22:34+01:00
logo: "fa-circle"
couleur: "vert"
image: "04.jpg"
weight: 4
draft: false
---

Je réalise des dessins techniques des différentes vues et coupes de l'article (face, profil...) pour en préciser chaque montage/type d'assemblage, en indiquant les zones de collage, de couture, de fixation d'accessoires.

Cela permet d'avoir une vue d'ensemble de l'article pour garder en mémoire/une trace de son assemblage, si je souhaite le réaliser par la suite en petite série. J'annote d'un numéro chaque pièce de l'article sur les dessins techniques, en correspondance avec la nomenclature, pour clarifier la lecture des documents réalisés.
