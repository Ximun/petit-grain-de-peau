.PHONY: help hugobuild

path=~/petitgraindepeau.art-e-toile.com/
ssh=artetoile@art-e-toile.com

help: ## Affiche cette aide
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

hugobuild: ## Déploi le site hugo à distance
	hugo
	rsync -avzhP ./public/ $(ssh):$(path)
